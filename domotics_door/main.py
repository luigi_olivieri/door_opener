from server import WebServer


def main():
    server = WebServer(debug=True, address="0.0.0.0", port=8080)
    server.run()


if __name__ == "__main__":
    main()
