from flask import Flask
import url_behavior


class WebServer:
    app: Flask = None

    def __init__(self, debug, address, port):
        self.app = Flask(__name__)
        self.debug: bool = debug
        self.address: str = address
        self.port: int = port

    def run(self):
        self.__set_url_behavior()
        self.app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
        self.app.run(debug = self.debug, host = self.address, port = self.port)

    def __set_url_behavior(self):
        self.app.add_url_rule("/", "authentication", url_behavior.authentication, methods=["GET", "POST"])
        self.app.add_url_rule("/open-door.html", "home", url_behavior.home, methods=["GET", "POST"])
        self.app.add_url_rule("/change-password.html", "change-password",
                              url_behavior.change_password, methods = ["GET", "POST"])
