from flask import render_template, request, redirect, flash, session
from os import sep
from encryption import encrypt, decode
import gpiozero


def authentication():
    if request.method == 'POST':
        if log_in_success(request.form.get('pass')):
            return redirect("open-door.html")

    return render_template("log-in.html")


def home():
    if request.method == "POST":
        open_door()

    return render_template("open-door.html")


def change_password():
    if request.method == 'POST':
        if log_in_success(str(request.form.get('old-pass'))) and request.form.get('new-pass') is not None:
            new_pass = encrypt(str(request.form.get('new-pass')))
            open('templates' + sep + 'static' + sep + 'pass.txt', '+w', encoding = 'utf-8').write(new_pass)
        return redirect("open-door.html")

    return render_template("change-password.html")


def log_in_success(password):
    expected = open('templates' + sep + 'static' + sep + 'pass.txt', encoding = 'utf-8').read()
    return decode(expected) == password


def open_door():
    # TODO open the door via electronics
    print("door opened")
