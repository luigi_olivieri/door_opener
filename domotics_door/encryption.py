worm = 'olivieri'
L = 200


def encrypt(password):
    encrypted_password = ''

    for i in range(len(password)):
        n = ord(worm[i]) + ord(password[i])
        r = int(n / L)
        letter = n - (L * r)
        encrypted_password += chr(letter)

    return encrypted_password


def decode(password):
    decoded_password = ''

    for i in range(len(password)):
        n = ord(password[i]) - ord(worm[i]) + L
        r = int(n / L)
        letter = n - (L * r)
        decoded_password += chr(letter)

    return decoded_password


